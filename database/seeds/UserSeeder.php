<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            //'name' => Str::random(10),
            //'email' => Str::random(10).'@gmail.com',
            'name' =>  'Super Admin',
            'email' => 'admin@correo.com',      
            'password' => Hash::make('54321'),
        ]);
    }
}
