
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '',
        component: () => 
            import('pages/index'),
            meta:{title: "home"}     
      }],
      meta:{ auth: true}
  },

  // Always leave this as last one,
  // but you can also remove it

  {
    path: '/login',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '',
        component: () => 
            import('pages/login'),
            meta:{title: "login"}     
      }],
      meta:{ auth: false}
  },
]

if (process.env.MODE !== 'ssr') {
  routes.push({
      path: '*',
      component: () =>
          import ('pages/system/Error404.vue')
  })
}
export default routes
