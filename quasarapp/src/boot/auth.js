import bearer from '@websanova/vue-auth/drivers/auth/bearer'
import axios from '@websanova/vue-auth/drivers/http/axios.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'

const config ={
    auth: bearer,
    http: axios,
    router: router,
    tokenDefaultName: 'token',
    //userDefaultName: 'default_auth_user',
    //rolesVar: 'roles',
    tokenStore: ['localStorage'],
    authRedirect: '/login',

    registerData: {
        url: 'auth/register', 
        method: 'POST', 
        redirect: '/login'
      },

    loginData: {
        url: '/auth/login',
        method: 'POST',
        redirect: '',
        //headerToken: 'Authorization',
        fetchUser: false,
        //(optional) customToken: (response) => response.data['token'],
      },
      logoutData: {
        url: '/auth/logout',
        method: 'POST',
        redirect: '/login',
        makeRequest: false,
      },
      fetchItem: '',
      fetchData: {
        url: '/auth/user',
        method: 'GET',
        //interval: 30,
        enabled: false,
      },
      refreshData: {
        url: '/auth/refresh',
        method: 'GET',
        interval: 30,
        enabled: true,
      }
}
export default config