import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$axios = axios
const axiosInstance = axios.create({ 
    baseURL: `${process.env.API_URL}api/v1`
    //baseURL: `http://localhost/api/v1`
  })

  Vue.$http = axiosInstance
Vue.prototype.$axios = axiosInstance
// NOTE only use in production mode
// instance.defaults.baseURL = `api/v1`
Vue.axios=axiosInstance

//console.log('error >',axiosInstance.defaults.baseURL)

export default ({ app, Vue, router}) => {
  Vue.prototype.$axios.interceptors.response.use( function(response){
    // console.log('responseFromInterceptor', response)
    return response
  }, function(error){
    console.log(' ------------ Error Interceptor Triggered ------------')
    console.log('error info >',error)
    console.log('error request config >',error.config)
    console.log('error response from backend >',error.response)
    console.log('--------------Error Interceptor Finish----------------')
 
    if(error.response.data.error === 'Unauthorized'){
      localStorage.removeItem('token')
      router.push('/login')
    }
  })
}