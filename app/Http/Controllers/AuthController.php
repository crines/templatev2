<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
//use Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        error_log("Login Ejecutado !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        if ($token = $this->guard()->attempt($credentials)) {
            error_log("entro");
            //return $this->respondWithToken($token);
            return response()->json([
                'status' => 'success',
                    'token' => $token], 200)->header('Authorization', $token);
        }
        error_log("no paso");
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        error_log("Refresh Ejecutado !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1 skdksamd");
        //return $this->respondWithToken($this->guard()->refresh());
        if(Auth::check()){
            if ($token = $this->guard()->refresh()) {
              return response()
                ->json([
                'status' => 'successs',
                'token' => $token
              ], 200)
                ->header('Authorization', $token);
            }
          }
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'status' => 'success',
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    public function user(Request $request){
        $user = User::find(Auth::user()->id);
        return response()->json([
          'status' => 'success',
          'data' => $user
        ],200);
      }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}
